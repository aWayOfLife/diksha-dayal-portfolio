import styles from "../styles/About.module.scss";
import { useRef, useEffect } from "react";
import Image from "next/image";
import { API_BASE_URL } from "../core/app.constants";
import ReactMarkdown from "react-markdown";

const About = ({ about }) => {
  const butterflyRef = useRef(null);
  const butterflyStyle = {
    width: "150%",
    height: "150%",
    position: "absolute",
    transform: "translateY(-40%) translateX(-20%)",
  };
  useEffect(() => {
    import("@lottiefiles/lottie-player");
  });

  return (
    <div className={styles.about}>
      <div className={styles.photo}>
        <h1>Diksha Dayal</h1>
        <Image
          loader={() =>
            about.data.attributes.photo.data.attributes.formats.large.url
          }
          src={about.data.attributes.photo.data.attributes.formats.large.url}
          alt="diksha-photo"
          layout="fill"
          objectFit="cover"
          unoptimized="true"
        ></Image>
      </div>
      <div className={styles.description}>
        <ReactMarkdown>{about.data.attributes.about}</ReactMarkdown>
        <div className={styles.butterfly}>
          <lottie-player
            id="butterfly"
            ref={butterflyRef}
            autoplay
            loop
            mode="normal"
            speed="0.5"
            src="https://assets4.lottiefiles.com/private_files/lf30_ohapmypv.json"
            style={butterflyStyle}
          ></lottie-player>
        </div>
      </div>
    </div>
  );
};

export default About;

export const getStaticProps = async () => {
  const about = await (
    await fetch(`${API_BASE_URL}/about?populate=photo`)
  ).json();

  return {
    props: {
      about,
    },
  };
};
