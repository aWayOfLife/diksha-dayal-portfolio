import Head from 'next/head'
import Introduction from '../components/Introduction'
import Works from '../components/Works'
import styles from '../styles/Home.module.scss'
import { API_BASE_URL } from '../core/app.constants'

export default function Home({ welcomeIntroduction, workItems }) {
  return (
    <div className={styles.container}>
      <Head>
        <title>Diksha Dayal</title>
        <meta name="description" content="Diksha Dayal | Portfolio | Designer" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Introduction welcomeIntroduction={welcomeIntroduction.data}></Introduction>
      <Works workItems={workItems?.data}></Works>
      <div>
      </div>
    </div>
  )
}

export const getStaticProps = async () => {
  const welcomeIntroduction = await (await fetch(`${API_BASE_URL}/welcome`)).json();
  const workItems = await (await fetch(`${API_BASE_URL}/works?populate=image`)).json();
  return {
    props: {
      welcomeIntroduction,
      workItems,
    },
  };
};
