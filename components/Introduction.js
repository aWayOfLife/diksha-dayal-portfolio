import styles from "../styles/Introduction.module.scss";
import { useRef, useEffect } from "react";
import Image from "next/image";
import Pattern from "./Pattern";

const Introduction = ({ welcomeIntroduction }) => {
  const welcome = `Hi, I'm Diksha Dayal,\nI design, dance, sing & create.`;
  const butterflyRef = useRef(null);
  const butterflyStyle = {
    width: "150%",
    height: "150%",
    position: "absolute",
    transform: "translateY(-40%) translateX(-20%)",
  };
  const patternStyle = {
    width: "550px",
    height: "550px",
    position: "absolute",
    transform: "translateY(-18%) translateX(-25%)",
  };
  useEffect(() => {
    import("@lottiefiles/lottie-player");
  });

  return (
    <div className={styles.introduction}>
      <h1>
        <Pattern className="pattern" style={patternStyle}></Pattern>
        <div className={styles.butterfly}>
          <lottie-player
            id="butterfly"
            ref={butterflyRef}
            autoplay
            loop
            mode="normal"
            speed="0.5"
            src="https://assets4.lottiefiles.com/private_files/lf30_ohapmypv.json"
            style={butterflyStyle}
          ></lottie-player>
        </div>
        {welcome}
      </h1>
      <p>{welcomeIntroduction.attributes.introduction}</p>
      <span className={styles.arrow}>
        <Image
          src="/assets/icons/arrow.svg"
          alt="arrow-down-icon"
          height="100%"
          width="100%"
        ></Image>
      </span>
    </div>
  );
};

export default Introduction;
