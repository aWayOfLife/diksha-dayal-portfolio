const Pattern = ({style}) => {
  return (
    <svg
      style={style}
      viewBox="0 0 675 675"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_f_211_410)">
        <circle
          cx="325.5"
          cy="349.5"
          r="325.5"
          fill="url(#paint0_linear_211_410)"
          fillOpacity="0.5"
        />
      </g>
      <defs>
        <filter
          id="filter0_f_211_410"
          x="-24"
          y="0"
          width="699"
          height="699"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="BackgroundImageFix"
            result="shape"
          />
          <feGaussianBlur
            stdDeviation="12"
            result="effect1_foregroundBlur_211_410"
          />
        </filter>
        <linearGradient
          id="paint0_linear_211_410"
          x1="326.091"
          y1="675"
          x2="324.909"
          y2="24"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#176BB9" stopOpacity="0" />
          <stop offset="1" stopColor="#FE5BDA" stopOpacity="0.36" />
        </linearGradient>
      </defs>
    </svg>
  );
};

export default Pattern;
