import Image from "next/image";
import { useState } from "react";
import styles from "../styles/Nav.module.scss";
import NavLink from "./NavLink";

const Nav = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  return (
    <nav className="container">
      <div className={styles.nav}>
        <div>
          <NavLink className={styles.logo} href="/">
            dd*
          </NavLink>
        </div>
        <ul className={isMenuOpen ? styles.open : null}>
          <li onClick={() => setIsMenuOpen(false)}>
            <NavLink exact href="/">
              Work.
            </NavLink>
          </li>
          <li onClick={() => setIsMenuOpen(false)}>
            <NavLink href="/about">About.</NavLink>
          </li>
          <li onClick={() => setIsMenuOpen(false)}>
            <a>Resume.</a>
          </li>
        </ul>
        <span
          className={styles.menu}
          onClick={() => setIsMenuOpen(!isMenuOpen)}
        >
          {isMenuOpen ? (
            <Image
              src="/assets/icons/close.svg"
              alt="menu-icon"
              width="25px"
              height="25px"
            ></Image>
          ) : (
            <Image
              src="/assets/icons/menu.svg"
              alt="menu-icon"
              width="25px"
              height="25px"
            ></Image>
          )}
        </span>
      </div>
    </nav>
  );
};

export default Nav;
