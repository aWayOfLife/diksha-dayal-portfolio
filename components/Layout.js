import styles from "../styles/Layout.module.scss";
import Nav from "./Nav";
import Underline from "./Underline";

const Layout = ({ children }) => {
  return (
    <>
      <Nav></Nav>
      <Underline></Underline>
      <main className="container">
        {children}
      </main>
    </>
  );
};

export default Layout;
