import styles from "../styles/Underline.module.scss";

const Underline = () => {
  return <div className={styles.underline}></div>;
};

export default Underline;
