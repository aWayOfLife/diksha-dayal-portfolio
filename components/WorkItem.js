import Image from "next/image";
import styles from "../styles/WorkItem.module.scss";

const WorkItem = ({ title, subtitle, image, url }) => {
  return (
    <div
      className={styles.item}
      onClick={() => {
        window.open(url, "_blank");
      }}
    >
      <Image
        className={styles.image}
        loader={() => image}
        src={image}
        alt="project-photo"
        layout="fill"
        objectFit="cover"
        objectPosition="0% 10%"
        unoptimized={true}
      ></Image>
      <div className={styles.overlay}>
        <h3>{title}</h3>
        <p>{subtitle}</p>
      </div>
    </div>
  );
};

export default WorkItem;
