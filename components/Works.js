import WorkItem from "./WorkItem";
import styles from "../styles/Works.module.scss";

const Works = ({ workItems }) => {
  return (
    <div className={styles.works}>
      <h2>Works.</h2>
      <div className={styles.items}>
        {workItems.map((item, index) => (
          <WorkItem
            key={index}
            title={item.attributes.title}
            subtitle={item.attributes.subtitle}
            image={item.attributes.image.data.attributes.formats.large.url}
            url={item.attributes.url}
          ></WorkItem>
        ))}
      </div>
    </div>
  );
};

export default Works;
